<p align="center">
  <img src="https://gitlab.com/zenphp/pinte/raw/HEAD/art/logo.svg" width="200" alt="Logo Pinte">
</p>

<p align="center">
  <a href="https://gitlab.com/zenphp/pinte/-/commits/main">
    <img src="https://gitlab.com/zenphp/pinte/badges/main/pipeline.svg" alt="pipeline status" />
  </a>
  <a href="https://packagist.org/packages/zenphp/pinte">
    <img src="https://img.shields.io/packagist/dt/zenphp/pinte" alt="Total Downloads">
  </a>
  <a href="https://packagist.org/packages/zenphp/pinte">
    <img src="https://img.shields.io/packagist/v/zenphp/pinte" alt="Latest Stable Version">
  </a>
  <a href="https://packagist.org/packages/zenphp/pinte">
    <img src="https://img.shields.io/packagist/l/zenphp/pinte" alt="License">
  </a>
</p>

<a name="introduction"></a>

## Introduction

**Pinte** is an less opinionated PHP code style fixer for minimalists. Pinte is built on top of **[PHP-CS-Fixer](https://gitlab.com/FriendsOfPHP/PHP-CS-Fixer)** and makes it simple to ensure that your code style stays **clean** and **consistent**.

Pinte is almost completely based on [Laravel Pint](https://gitlab.com/laravel/pint) with the added configuration (to start) of indentation and line endings which are a much requested feature.

If there are other configuration additions you'd like added please feel free to open an issue or a pull request.

## Official Documentation

### Installation

```bash
$ composer require zenphp/pinte --dev
```

### Usage

Use the same commands as Pint:

```bash
$ php ./vendor/bin/pinte
```

or

```bash
$ php ./vendor/bin/pinte --dirty
```

To change the indentation, add the indent option to `pinte.json`:

```json
{
  "indent": "\t"
}
```

Default for intentation is **4 spaces.**

To change the line ending option:

```json
{
  "lineEnding": "\r\n"
}
```

Default line ending is a single **newline.**

> Further documentation for Pinte is forthcoming.

<a name="security-vulnerabilities"></a>

## Security Vulnerabilities

Please review [our security policy](https://gitlab.com/zenphp/pinte/security/policy) on how to report security vulnerabilities.

<a name="license"></a>

## License

Pinte is open-sourced software licensed under the [MIT license](LICENSE.md).
