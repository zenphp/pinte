# Release Notes

# [1.2.0](https://gitlab.com/zenphp/pinte/compare/v1.1.0...v1.2.0) (2024-03-07)


### Features

* optimizing for release management ([b6f3663](https://gitlab.com/zenphp/pinte/commit/b6f3663ffaa1cdaf34c7717488c9c8f34c1ba0a9))


### Maintenance

* fix matrix ([6d54b8b](https://gitlab.com/zenphp/pinte/commit/6d54b8b12dbb1f0cb0e86de1256e5f0abc806388))
* remove build step ([d2daf2b](https://gitlab.com/zenphp/pinte/commit/d2daf2b864b58041b22a050184b3ca61cd2b3fd5))
* remove matrix for 8.1 ([39aa21e](https://gitlab.com/zenphp/pinte/commit/39aa21e70ecea531ac317340bb167cc69ea08182))
* removed psr12 preset ([3b16663](https://gitlab.com/zenphp/pinte/commit/3b1666304f486b205be70e802d45d08db2392855))
* styles ([f363787](https://gitlab.com/zenphp/pinte/commit/f36378743c74f8bb832de67bf253a27dd8ab9e1c))
